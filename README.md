# What is LazVCSHelper?

LazVCSHelper is an IDE plugin for the [Lararus IDE](http://www.lazarus-ide.org/) that simplifies using a Version Control System (VCS) for FreePascal projects.

![Screenshot](doc/laz-git-helper-screenshot.png)

It looks for installed instances of TortoiseGit and/or TortoiseSVN and if found, adds an item to the main menu for each that offers the following Tortoise commands:

* Commit
* Push
* Pull
* Revert current file
* Diff all
* Diff current file
* Log all
* Log current file (new in 0.2.3)
* Browser
* Init
* Clone
* Create Tag (new in 0.2.3)

The name results from the plugin first being available for Git only. Currently supported VCS are:

* Git through [TortoiseGit](https://tortoisegit.org/)
* Subversion through [TortoiseSVN](https://tortoisesvn.net/)
* Mercurial through [TortoiseHg] (https://tortoisehg.bitbucket.io/)
* Bazaar through [TortoiseBzr] (http://wiki.bazaar.canonical.com/TortoiseBzr)
                     
# Installation

## Prerequisites

1. Install [TortoiseGit](https://tortoisegit.org/) if you want Git support.
2. Install [TortoiseSVN](https://tortoisesvn.net/) if you want Subversion support.
3. Install [TortoiseHg] (https://tortoisehg.bitbucket.io/) if you want Mercurial support.
4. Install [TortoiseBzr] (http://wiki.bazaar.canonical.com/TortoiseBzr) if you want Bazaar support.

## Final steps

1. Download or check out files.
2. Open `VCSHelperPackage.lpk` in Lazarus.
3. When asked, tell Lazarus to *Open Package*
4. Choose *Install* from the *Use >>* button menu.
5. It will ask you to rebuild Lazarus, allow it do so. After Lazarus has been restarted, one or two additional menus will appear, depending on the installed prerequisites.