{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Classes for new IDE menus.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017-2022 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2022-05-04  pk  ---  Added "Create Tag..." option
// 2022-05-04  pk  ---  Added "Log current..." option
// 2017-05-10  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit VCSHelper.Menu;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$codepage utf8}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Dialogs,
   Forms, // for TModalResult
   Controls,
   Menus,
   MenuIntf,
   SrcEditorIntf,
   ProjectIntf,
   LazIDEIntf,
   LazarusPackageIntf,
   IDEOptEditorIntf,
   IDEOptionsIntf,
   Registry,
   ShellApi,
   VCSHelper.Strings,
   VCSHelper.Options;

type

   TSupportedVCSAction = (vcsaBrowse, vcsaClone, vcsaCommit, vcsaDiff, vcsaInit, vcsaLog, vcsaPull, vcsaPush, vcsaRevert, vcsaSettings, vcsaRevisionGraph, vcsaCreateTag);
   TSupportedVCSActions = set of TSupportedVCSAction;
   TSupportedVCSActionsCommands = array[TSupportedVCSAction] of string;

   { TLazarusVCSHelper }

   TLazarusVCSHelper = class
   private
      FParameterCommandFormat: string;
      FParameterFileFormat: string;
      FSettings: TVCSBaseSettings;
      FSubFolderName: string;
      FSupportedActions: TSupportedVCSActions;
      FTortoiseExecutable: string;
      FVCSName: string;
      FIdentificationRegistryKey: string;
      FTortoiseFilename: string;
      FTortoiseCommands: TSupportedVCSActionsCommands;
      FBasePath: string;
      FMenuCommandAbout: TIDEMenuCommand;
      FMenuCommandBrowse: TIDEMenuCommand;
      FMenuCommandClone: TIDEMenuCommand;
      FMenuCommandCommit: TIDEMenuCommand;
      FMenuCommandDiff: TIDEMenuCommand;
      FMenuCommandDiffAll: TIDEMenuCommand;
      FMenuCommandInit: TIDEMenuCommand;
      FMenuCommandCreateTag: TIDEMenuCommand;
      FMenuCommandLog: TIDEMenuCommand;
      FMenuCommandLogAll: TIDEMenuCommand;
      FMenuCommandPull: TIDEMenuCommand;
      FMenuCommandPush: TIDEMenuCommand;
      FMenuCommandRevert: TIDEMenuCommand;
      FMenuCommandSettings: TIDEMenuCommand;
      FMenuCommandRevisionGraph: TIDEMenuCommand;
      FMenuItemRepoPath: TIDEMenuCommand;
      FMenuSection: TIDEMenuSection;
      FMenuSectionAbout: TIDEMenuSection;
      FMenuSectionActiveWork: TIDEMenuSection;
      FMenuSectionSetup: TIDEMenuSection;
      FMenuSectionStatus: TIDEMenuSection;
      procedure AddHandlers();
      procedure RemoveHandlers();
      function GetTortoiseCommand(AVCSAction: TSupportedVCSAction): string;
      procedure SetIdentificationRegistryKey(AValue: string);
      procedure SetTortoiseCommand(AVCSAction: TSupportedVCSAction; AValue: string);
      function TestTortoiseInstalled: boolean;
      procedure ExecuteTortoiseCommand(ACommand: string; AUsePath: boolean = True; ACustomPath: string = '');
      function GetWorkingPath(var APath: string): boolean;
   protected
      procedure DoIDEClose({%H-}Sender: TObject);
      function DoProjectOpened({%H-}Sender: TObject; AProject: TLazProject): TModalResult;
      function DoProjectClose({%H-}Sender: TObject; {%H-}AProject: TLazProject): TModalResult;
   protected
      procedure FindBasePath(AStartPath: string);
      procedure SetStatusForPath(APath: string);
   public
      constructor Create();
      function IsTortoiseInstalled: boolean;
      procedure CreateMainMenuSubMenu();
      procedure About({%H-}Sender: TObject);
      procedure Browse({%H-}Sender: TObject);
      procedure Clone({%H-}Sender: TObject);
      procedure CreateTag({%H-}Sender: TObject);
      procedure Commit({%H-}Sender: TObject);
      procedure DiffCurrent({%H-}Sender: TObject);
      procedure DiffAll({%H-}Sender: TObject);
      procedure Init({%H-}Sender: TObject);
      procedure LogAll({%H-}Sender: TObject);
      procedure LogCurrent({%H-}Sender: TObject);
      procedure MenuOpen({%H-}Sender: TObject);
      procedure Pull({%H-}Sender: TObject);
      procedure Push({%H-}Sender: TObject);
      procedure RevertCurrent({%H-}Sender: TObject);
      procedure RevisionGraph({%H-}Sender: TObject);
      procedure ShowSettings({%H-}Sender: TObject);
      procedure ToggleMenuVisible(AVisible: boolean);
      property ParameterCommandFormat: string read FParameterCommandFormat write FParameterCommandFormat;
      property ParameterFileFormat: string read FParameterFileFormat write FParameterFileFormat;
      property SubFolderName: string read FSubFolderName write FSubFolderName;
      property TortoiseExecutable: string read FTortoiseExecutable write FTortoiseExecutable;
      property TortoiseCommands[AVCSAction: TSupportedVCSAction]: string read GetTortoiseCommand write SetTortoiseCommand;
      property VCSName: string read FVCSName write FVCSName;
      property IdentificationRegistryKey: string read FIdentificationRegistryKey write SetIdentificationRegistryKey;
      property SupportedActions: TSupportedVCSActions read FSupportedActions write FSupportedActions;
      property Settings: TVCSBaseSettings read FSettings write FSettings;
   end;

   { TLazarusGitHelper }

   TLazarusGitHelper = class(TLazarusVCSHelper)
   public
      constructor Create();
   end;

   { TLazarusSubversionHelper }

   TLazarusSubversionHelper = class(TLazarusVCSHelper)
   public
      constructor Create();
   end;

   { TLazarusMercurialHelper }

   TLazarusMercurialHelper = class(TLazarusVCSHelper)
   public
      constructor Create();
   end;

   { TLazarusBazaarHelper }

   TLazarusBazaarHelper = class(TLazarusVCSHelper)
   public
      constructor Create();
   end;

procedure Register;

var
   GBazaarHelper: TLazarusBazaarHelper = nil;
   GGitHelper: TLazarusGitHelper = nil;
   GMercurialVersionHelper: TLazarusMercurialHelper = nil;
   GSubVersionHelper: TLazarusSubversionHelper = nil;

implementation

uses
   LCLVersion;

procedure Register;
begin
   if GBazaarHelper.IsTortoiseInstalled then begin
      GBazaarHelper.CreateMainMenuSubMenu();
   end;
   if GGitHelper.IsTortoiseInstalled then begin
      GGitHelper.CreateMainMenuSubMenu();
   end;
   if GMercurialVersionHelper.IsTortoiseInstalled then begin
      GMercurialVersionHelper.CreateMainMenuSubMenu();
   end;
   if GSubVersionHelper.IsTortoiseInstalled then begin
      GSubVersionHelper.CreateMainMenuSubMenu();
   end;
end;

const
   SVCSDefaultActionsParameter: TSupportedVCSActionsCommands = ('browse', 'clone', 'commit', 'diff', 'init', 'log', 'pull', 'push', 'revert', 'settings', 'revisiongraph', 'createtag');

constructor TLazarusBazaarHelper.Create;
begin
   SubFolderName := '.bzr';
   VCSName := 'Bazaar';
   TortoiseExecutable := 'tbzrcommand.exe';
   IdentificationRegistryKey := '\SOFTWARE\Bazaar\';
   SupportedActions := [vcsaBrowse, vcsaClone, vcsaCommit, vcsaDiff, vcsaInit, vcsaLog, vcsaPull, vcsaPush, vcsaRevert];
   ParameterCommandFormat := '--command=%s';
   ParameterFileFormat := ' --file=%s';
   Settings := VCSSettings.BazaarSettings;
end;

constructor TLazarusMercurialHelper.Create;
begin
   inherited Create();
   SubFolderName := '.hg';
   VCSName := 'Mercurial';
   TortoiseExecutable := 'thg.exe';
   TortoiseCommands[vcsaDiff] := 'vdiff';
   IdentificationRegistryKey := '\SOFTWARE\TortoiseHg\';
   SupportedActions := [vcsaClone, vcsaCommit, vcsaDiff, vcsaInit, vcsaLog, vcsaRevert];
   ParameterCommandFormat := '%s';
   ParameterFileFormat := ' %s';
   Settings := VCSSettings.MercurialSettings;
end;

{ TLazarusGitHelper }

constructor TLazarusGitHelper.Create;
begin
   inherited Create();
   SubFolderName := '.git';
   VCSName := 'Git';
   IdentificationRegistryKey := '\SOFTWARE\TortoiseGit\';
   SupportedActions := [vcsaBrowse, vcsaClone, vcsaCommit, vcsaDiff, vcsaInit, vcsaLog, vcsaPull, vcsaPush, vcsaRevert, vcsaSettings, vcsaRevisionGraph, vcsaCreateTag];
   ParameterCommandFormat := '/command:%s';
   ParameterFileFormat := ' /path:%s';
   Settings := VCSSettings.GitSettings;
end;

{ TLazarusSubversionHelper }

constructor TLazarusSubversionHelper.Create;
begin
   inherited Create();
   SubFolderName := '.svn';
   VCSName := 'Subversion';
   IdentificationRegistryKey := '\SOFTWARE\TortoiseSVN\';
   SupportedActions := [vcsaBrowse, vcsaClone, vcsaCommit, vcsaDiff, vcsaInit, vcsaLog, vcsaPull, vcsaPush, vcsaRevert, vcsaSettings, vcsaRevisionGraph];
   ParameterCommandFormat := 'command';
   ParameterFileFormat := ' /path:%s';
   Settings := VCSSettings.SubversionSettings;
end;

{ TGitHelper }

function TLazarusVCSHelper.TestTortoiseInstalled: boolean;
var
   reg: TRegistry;
begin
   // This will result in a path like this one:
   // 'C:\Program Files\TortoiseGit\bin\TortoiseGitProc.exe';
   // 'C:\Program Files\TortoiseSVN\bin\TortoiseSVNProc.exe';
   Result := False;
   try
      FTortoiseFilename := '';
      reg := TRegistry.Create;
      try
         reg.RootKey := HKEY_LOCAL_MACHINE;
         if reg.OpenKeyReadOnly(FIdentificationRegistryKey) then begin
            try
               if reg.ValueExists('ProcPath') then begin
                  FTortoiseFilename := reg.ReadString('ProcPath');
                  Result := FileExists(FTortoiseFilename);
               end else if reg.ValueExists('InstallPath') then begin
                  FTortoiseFilename := IncludeTrailingPathDelimiter(reg.ReadString('InstallPath')) + FTortoiseExecutable;
                  Result := FileExists(FTortoiseFilename);
               end else begin
                  FTortoiseFilename := IncludeTrailingPathDelimiter(reg.ReadString('')) + FTortoiseExecutable;
                  Result := FileExists(FTortoiseFilename);
               end;
            finally
               reg.CloseKey;
            end;
         end;
      finally
         reg.Free;
      end;
   except
      on E: Exception do begin
         Exit;
      end;
   end;
end;

procedure TLazarusVCSHelper.SetIdentificationRegistryKey(AValue: string);
begin
   FIdentificationRegistryKey := AValue;
   TestTortoiseInstalled;
end;

function TLazarusVCSHelper.GetTortoiseCommand(AVCSAction: TSupportedVCSAction): string;
begin
   Result := FTortoiseCommands[AVCSAction];
end;

procedure TLazarusVCSHelper.SetTortoiseCommand(AVCSAction: TSupportedVCSAction; AValue: string);
begin
   FTortoiseCommands[AVCSAction] := AValue;
end;

function TLazarusVCSHelper.IsTortoiseInstalled: boolean;
begin
   Result := Length(FTortoiseFilename) > 0;
   if Result then begin
      Result := FileExists(FTortoiseFilename);
   end;
end;

procedure TLazarusVCSHelper.ExecuteTortoiseCommand(ACommand: string; AUsePath: boolean; ACustomPath: string);
var
   sParameters: string;
begin
   if DirectoryExists(FBasePath + FSubFolderName) then begin
      sParameters := Format(FParameterCommandFormat, [ACommand]);
      if AUsePath then begin
         if Length(ACustomPath) > 0 then begin
            sParameters += Format(FParameterFileFormat, [ACustomPath]);
         end else begin
            sParameters += Format(FParameterFileFormat, [FBasePath]);
         end;
      end;
      sParameters += Format(' /closeonend:%d', [Settings.CloseOnEnd]);
      ShellExecute(0, 'open', PChar(FTortoiseFilename), PChar(sParameters), PChar(FBasePath), 1);
   end else begin
      MessageDlg(rsVCSHelperTitle, Format(rsVCSNoRepositoryFoundText, [FVCSName]),
         mtError, [mbAbort], 0);
   end;
end;

function TLazarusVCSHelper.GetWorkingPath(var APath: string): boolean;
begin
   APath := ExtractFilePath(LazarusIDE.ActiveProject.ProjectInfoFile);
   FMenuSection.Hint := APath;
   FMenuItemRepoPath.Caption := APath;
   Result := True;
end;

procedure TLazarusVCSHelper.DoIDEClose(Sender: TObject);
begin
   RemoveHandlers();
end;

function TLazarusVCSHelper.DoProjectOpened(Sender: TObject; AProject: TLazProject): TModalResult;
var
   sPath: string;
begin
   sPath := ExtractFilePath(AProject.ProjectInfoFile);
   SetStatusForPath(sPath);
   Result := mrOk;
end;

function TLazarusVCSHelper.DoProjectClose(Sender: TObject; AProject: TLazProject): TModalResult;
begin
   if not Assigned(Settings) then begin
      Exit;
   end;
   if DirectoryExists(FBasePath + FSubFolderName) and (Settings.CommitOnProjectClose) then begin
      ExecuteTortoiseCommand('commit', False);
   end;
   Result := mrOk;
end;

procedure TLazarusVCSHelper.FindBasePath(AStartPath: string);
var
   sl: TStringList;
   sTempPath: string;
begin
   FBasePath := IncludeTrailingPathDelimiter(AStartPath);
   sl := TStringList.Create;
   try
      sl.StrictDelimiter := True;
      sl.Delimiter := '\';
      sl.DelimitedText := AStartPath;
      while sl.Count > 1 do begin
         sTempPath := IncludeTrailingPathDelimiter(sl.DelimitedText);
         if DirectoryExists(sTempPath + FSubFolderName) then begin
            FBasePath := sTempPath;
            break;
         end;
         sl.Delete(Pred(sl.Count));
      end;
   finally
      sl.Free;
   end;
   FMenuItemRepoPath.Caption := '@' + FBasePath;
end;

procedure TLazarusVCSHelper.CreateMainMenuSubMenu;
begin
   AddHandlers();
   FMenuSection := RegisterIDESubMenu(mnuMain, FVCSName + 'Menu', FVCSName);
   FMenuSection.Enabled := IsTortoiseInstalled;
   FMenuSection.OnClick := @MenuOpen;
   ToggleMenuVisible(Settings.ShowInMainMenu);

   FMenuSectionActiveWork := RegisterIDEMenuSection(FMenuSection, FVCSName + 'ActiveWork');
   FMenuItemRepoPath := RegisterIDEMenuCommand(FMenuSectionActiveWork, FVCSName + 'RepoPath', '???');
   FMenuItemRepoPath.Enabled := False;
   if vcsaCommit in SupportedActions then begin
      FMenuCommandCommit := RegisterIDEMenuCommand(FMenuSectionActiveWork, FVCSName + 'Commit', rsVCSActionCommit, @Commit);
   end else begin
      FMenuCommandCommit := nil;
   end;
   if vcsaPush in SupportedActions then begin
      FMenuCommandPush := RegisterIDEMenuCommand(FMenuSectionActiveWork, FVCSName + 'Push', rsVCSActionPush, @Push);
   end else begin
      FMenuCommandPush := nil;
   end;
   if vcsaPull in SupportedActions then begin
      FMenuCommandPull := RegisterIDEMenuCommand(FMenuSectionActiveWork, FVCSName + 'Pull', rsVCSActionPull, @Pull);
   end else begin
      FMenuCommandPull := nil;
   end;
   if vcsaRevert in SupportedActions then begin
      FMenuCommandRevert := RegisterIDEMenuCommand(FMenuSectionActiveWork, FVCSName + 'Revert', rsVCSActionRevertCurrent, @RevertCurrent);
   end else begin
      FMenuCommandRevert := nil;
   end;

   FMenuSectionStatus := RegisterIDEMenuSection(FMenuSection, FVCSName + 'Status');
   if vcsaDiff in SupportedActions then begin
      FMenuCommandDiffAll := RegisterIDEMenuCommand(FMenuSectionStatus, FVCSName + 'DiffAll', rsVCSActionDiffAll, @DiffAll);
      FMenuCommandDiff := RegisterIDEMenuCommand(FMenuSectionStatus, FVCSName + 'Diff', rsVCSActionDiffCurrent, @DiffCurrent);
   end else begin
      FMenuCommandDiffAll := nil;
      FMenuCommandDiff := nil;
   end;
   if vcsaLog in SupportedActions then begin
      FMenuCommandLogAll := RegisterIDEMenuCommand(FMenuSectionStatus, FVCSName + 'LogAll', rsVCSActionLog, @LogAll);
      FMenuCommandLog := RegisterIDEMenuCommand(FMenuSectionStatus, FVCSName + 'Log', rsVCSActionLog, @LogCurrent);
   end else begin
      FMenuCommandLogAll := nil;
      FMenuCommandLog := nil;
   end;
   if vcsaBrowse in SupportedActions then begin
      FMenuCommandBrowse := RegisterIDEMenuCommand(FMenuSectionStatus, FVCSName + 'Browse', rsVCSActionBrowse, @Browse);
   end else begin
      FMenuCommandBrowse := nil;
   end;
   if vcsaRevisionGraph in SupportedActions then begin
      FMenuCommandRevisionGraph := RegisterIDEMenuCommand(FMenuSectionStatus, FVCSName + 'RevisionGraph', rsVCSRevisionGraph, @RevisionGraph);
   end else begin
      FMenuCommandRevisionGraph := nil;
   end;

   FMenuSectionSetup := RegisterIDEMenuSection(FMenuSection, FVCSName + 'Status');
   if vcsaInit in SupportedActions then begin
      FMenuCommandInit := RegisterIDEMenuCommand(FMenuSectionSetup, FVCSName + 'Init', rsVCSActionInit, @Init);
   end else begin
      FMenuCommandInit := nil;
   end;
   if vcsaClone in SupportedActions then begin
      FMenuCommandClone := RegisterIDEMenuCommand(FMenuSectionSetup, FVCSName + 'Clone', rsVCSActionClone, @Clone);
   end else begin
      FMenuCommandClone := nil;
   end;
   if vcsaCreateTag in SupportedActions then begin
      FMenuCommandCreateTag := RegisterIDEMenuCommand(FMenuSectionSetup, FVCSName + 'CreateTag', rsVCSActionCreateTag, @CreateTag);
   end else begin
      FMenuCommandCreateTag := nil;
   end;

   FMenuSectionAbout := RegisterIDEMenuSection(FMenuSection, FVCSName + 'AboutMenu');
   FMenuCommandAbout := RegisterIDEMenuCommand(FMenuSectionAbout, FVCSName + 'About', rsVCSActionAbout, @About);
   if vcsaSettings in SupportedActions then begin
      FMenuCommandSettings := RegisterIDEMenuCommand(FMenuSectionAbout, FVCSName + 'Settings', rsVCSActionSettings, @ShowSettings);
   end else begin
      FMenuCommandSettings := nil;
   end;
end;

procedure TLazarusVCSHelper.AddHandlers;
begin
   LazarusIDE.AddHandlerOnProjectOpened(@DoProjectOpened);
   LazarusIDE.AddHandlerOnProjectClose(@DoProjectClose);
   LazarusIDE.AddHandlerOnIDEClose(@DoIDEClose);
end;

procedure TLazarusVCSHelper.RemoveHandlers;
begin
   LazarusIDE.RemoveHandlerOnProjectOpened(@DoProjectOpened);
   LazarusIDE.RemoveHandlerOnProjectClose(@DoProjectClose);
   LazarusIDE.RemoveHandlerOnIDEClose(@DoIDEClose);
end;

procedure TLazarusVCSHelper.SetStatusForPath(APath: string);
var
   bIsRepository: boolean;
begin
   FindBasePath(APath);
   bIsRepository := DirectoryExists(FBasePath + FSubFolderName);
   FMenuSectionActiveWork.Enabled := bIsRepository;
   if Assigned(FMenuCommandCommit) then begin
      FMenuCommandCommit.Enabled := bIsRepository;
   end;
   if Assigned(FMenuCommandPush) then begin
      FMenuCommandPush.Enabled := bIsRepository;
   end;
   if Assigned(FMenuCommandPull) then begin
      FMenuCommandPull.Enabled := bIsRepository;
   end;

   FMenuSectionStatus.Enabled := bIsRepository;
   if Assigned(FMenuCommandDiffAll) then begin
      FMenuCommandDiffAll.Enabled := bIsRepository;
   end;
   if Assigned(FMenuCommandDiff) then begin
      FMenuCommandDiff.Enabled := bIsRepository and Assigned(SourceEditorManagerIntf.ActiveEditor);
   end;
   if Assigned(FMenuCommandRevert) then begin
      FMenuCommandRevert.Enabled := bIsRepository and Assigned(SourceEditorManagerIntf.ActiveEditor);
   end;
   if Assigned(FMenuCommandLog) then begin
      FMenuCommandLog.Enabled := bIsRepository;
   end;
   if Assigned(FMenuCommandBrowse) then begin
      FMenuCommandBrowse.Enabled := bIsRepository;
   end;
   if Assigned(FMenuCommandRevisionGraph) then begin
      FMenuCommandRevisionGraph.Enabled := bIsRepository;
   end;

   if Assigned(FMenuSectionSetup) then begin
      FMenuSectionSetup.Enabled := not bIsRepository;
   end;
   if Assigned(FMenuCommandInit) then begin
      FMenuCommandInit.Enabled := not bIsRepository;
   end;
   if Assigned(FMenuCommandClone) then begin
      FMenuCommandClone.Enabled := not bIsRepository;
   end;
end;

constructor TLazarusVCSHelper.Create;
begin
   TestTortoiseInstalled;
   FTortoiseCommands := SVCSDefaultActionsParameter;
end;

procedure TLazarusVCSHelper.MenuOpen(Sender: TObject);
var
   edit: TSourceEditorInterface;
begin
   edit := SourceEditorManagerIntf.ActiveEditor;
   if Assigned(edit) then begin
      if Assigned(FMenuCommandRevert) then begin
         FMenuCommandRevert.Caption := Format(rsVCSActionRevertSpecific, [ExtractFileName(edit.FileName)]);
      end;
      if Assigned(FMenuCommandDiff) then begin
         FMenuCommandDiff.Caption := Format(rsVCSActionDiffSpecific, [ExtractFileName(edit.FileName)]);
      end;
      if Assigned(FMenuCommandLog) then begin
         FMenuCommandLog.Caption := Format(rsVCSActionLogSpecific, [ExtractFileName(edit.FileName)]);
      end;
   end else begin
      if Assigned(FMenuCommandRevert) then begin
         FMenuCommandRevert.Caption := rsVCSActionRevertCurrent;
         FMenuCommandRevert.Enabled := False;
      end;
      if Assigned(FMenuCommandDiff) then begin
         FMenuCommandDiff.Caption := rsVCSActionDiffCurrent;
         FMenuCommandDiff.Enabled := False;
      end;
         if Assigned(FMenuCommandLog) then begin
            FMenuCommandLog.Caption := rsVCSActionLogCurrent;
            FMenuCommandLog.Enabled := False;
         end;
   end;
end;

procedure TLazarusVCSHelper.Init(Sender: TObject);
begin
   ExecuteTortoiseCommand('repocreate');
end;

procedure TLazarusVCSHelper.Clone(Sender: TObject);
begin
   ExecuteTortoiseCommand('clone');
end;

procedure TLazarusVCSHelper.CreateTag(Sender: TObject);
begin
   ExecuteTortoiseCommand('tag');
end;

procedure TLazarusVCSHelper.Commit(Sender: TObject);
begin
   LazarusIDE.DoSaveProject([]);
   LazarusIDE.DoSaveAll([]);
   ExecuteTortoiseCommand('commit', False);
end;

procedure TLazarusVCSHelper.DiffAll(Sender: TObject);
begin
   ExecuteTortoiseCommand('diff', True);
end;

procedure TLazarusVCSHelper.DiffCurrent(Sender: TObject);
var
   edit: TSourceEditorInterface;
begin
   edit := SourceEditorManagerIntf.ActiveEditor;
   if Assigned(edit) then begin
      ExecuteTortoiseCommand('diff', True, edit.FileName);
   end;
end;

procedure TLazarusVCSHelper.RevertCurrent(Sender: TObject);
var
   edit: TSourceEditorInterface;
begin
   edit := SourceEditorManagerIntf.ActiveEditor;
   if Assigned(edit) then begin
      ExecuteTortoiseCommand('revert', True, edit.FileName);
   end;
end;

procedure TLazarusVCSHelper.RevisionGraph(Sender: TObject);
begin
   ExecuteTortoiseCommand('revisiongraph', True);
end;

procedure TLazarusVCSHelper.ShowSettings(Sender: TObject);
begin
   ExecuteTortoiseCommand('settings', False);
end;

procedure TLazarusVCSHelper.ToggleMenuVisible(AVisible: boolean);
begin
   if Assigned(FMenuSection) then begin
      FMenuSection.Enabled := AVisible;
      FMenuSection.Visible := AVisible;
      // The following is a workaround for Lazarus 1.6.4
      // See https://bugs.freepascal.org/view.php?id=31970
      // See http://forum.lazarus.freepascal.org/index.php/topic,37086.0.html
      //if AVisible then begin
      //   FMenuSection.Caption := FVCSName;
      //end else begin
      //   FMenuSection.Caption := ' ';
      //end;
   end;
end;

procedure TLazarusVCSHelper.LogAll(Sender: TObject);
begin
   ExecuteTortoiseCommand('log');
end;

procedure TLazarusVCSHelper.LogCurrent(Sender: TObject);
var
   edit: TSourceEditorInterface;
begin
   edit := SourceEditorManagerIntf.ActiveEditor;
   if Assigned(edit) then begin
      ExecuteTortoiseCommand('log', True, edit.FileName);
   end;
end;

procedure TLazarusVCSHelper.Push(Sender: TObject);
begin
   ExecuteTortoiseCommand('push', False);
end;

procedure TLazarusVCSHelper.Pull(Sender: TObject);
begin
   ExecuteTortoiseCommand('pull', False);
end;

procedure TLazarusVCSHelper.About(Sender: TObject);
begin
   MessageDlg(rsVCSHelperTitle, 'LazVCSHelper © 2016-2022 Patrick Kolla-ten Venne'#13#10'GPL v3', mtInformation, [mbOK], 0);
end;

procedure TLazarusVCSHelper.Browse(Sender: TObject);
begin
   ExecuteTortoiseCommand('repobrowser', False);
end;

initialization

   VCSOptionGroup := GetFreeIDEOptionsGroupIndex(GroupEditor);
   RegisterIDEOptionsGroup(VCSOptionGroup, TVCSSettings);
   GGitHelper := TLazarusGitHelper.Create();
   GBazaarHelper := TLazarusBazaarHelper.Create();
   GMercurialVersionHelper := TLazarusMercurialHelper.Create();
   GSubVersionHelper := TLazarusSubversionHelper.Create();

finalization

   GGitHelper.Free;
   GBazaarHelper.Free;
   GMercurialVersionHelper.Free;
   GSubVersionHelper.Free;
end.

