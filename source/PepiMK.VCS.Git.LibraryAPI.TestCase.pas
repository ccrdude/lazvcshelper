{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-23  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit PepiMK.VCS.Git.LibraryAPI.TestCase;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   fpcunit,
   testutils,
   testregistry,
   Windows,
   PepiMK.VCS.Git.LibraryAPI;

type

   { TTestCaseGitLibrary }

   TTestCaseGitLibrary = class(TTestCase)
   private
      FOnGitLibraryNotFoundCalled: boolean;
      function GetTestRepoPath: AnsiString;
      function DoGitLibraryNotFoundWithFix(AGitLibrary: TGitLibrary; var AFilename: WideString): boolean;
      function DoGitLibraryNotFoundWithoutFix({%H-}AGitLibrary: TGitLibrary; var {%H-}AFilename: WideString): boolean;
   published
      procedure TestInit;
      procedure TestOnGitLibraryNotFoundWithFix;
      procedure TestOnGitLibraryNotFoundWithoutFix;
      procedure TestGitRepositoryOpenFree;
      procedure TestGitDiffTreeToWorkdirWithIndex;
   end;

implementation

function TTestCaseGitLibrary.GetTestRepoPath: AnsiString;
begin
   Result := 'c:\Development\Projects\CodingTools\laz-vcs-helper\';
end;

function TTestCaseGitLibrary.DoGitLibraryNotFoundWithFix(AGitLibrary: TGitLibrary; var AFilename: WideString): boolean;
begin
   FOnGitLibraryNotFoundCalled := True;
   AFilename := AGitLibrary.GetDefaultFilename;
   Result := true;
end;

function TTestCaseGitLibrary.DoGitLibraryNotFoundWithoutFix(AGitLibrary: TGitLibrary; var AFilename: WideString): boolean;
begin
   FOnGitLibraryNotFoundCalled := True;
   Result := true;
end;

procedure TTestCaseGitLibrary.TestInit;
var
   g: TGitLibrary;
begin
   g := TGitLibrary.Create();
   try
      CheckTrue(g.Init, 'TGitLibrary.Init');
      CheckTrue(glsInitialized = g.Status, 'TGitLibrary.Status = glsInitialized');
   finally
      g.Free;
   end;
end;

procedure TTestCaseGitLibrary.TestOnGitLibraryNotFoundWithFix;
var
   g: TGitLibrary;
begin
   FOnGitLibraryNotFoundCalled := False;
   g := TGitLibrary.Create('blubbels.dll');
   try
      g.OnGitLibraryNotFound := @DoGitLibraryNotFoundWithFix;
      CheckTrue(g.Init, 'TGitLibrary.Init');
      CheckTrue(FOnGitLibraryNotFoundCalled);
   finally
      g.Free;
   end;
end;

procedure TTestCaseGitLibrary.TestOnGitLibraryNotFoundWithoutFix;
var
   g: TGitLibrary;
begin
   FOnGitLibraryNotFoundCalled := False;
   g := TGitLibrary.Create('blubbels.dll');
   try
      g.OnGitLibraryNotFound := @DoGitLibraryNotFoundWithoutFix;
      CheckFalse(g.Init, 'TGitLibrary.Init');
      CheckTrue(FOnGitLibraryNotFoundCalled);
   finally
      g.Free;
   end;
end;

procedure TTestCaseGitLibrary.TestGitRepositoryOpenFree;
var
   g: TGitLibrary;
   r: PGitRepository;
   b: boolean;
begin
   g := TGitLibrary.Create();
   try
      CheckTrue(g.Init, 'TGitLibrary.Init');
      CheckTrue(glsInitialized = g.Status, 'TGitLibrary.Status = glsInitialized');
      r := nil;
      b := g.OpenRepository(r, GetTestRepoPath);
      CheckTrue(b, 'TGitLibrary.OpenRepository');
      if b then begin
         CheckTrue(g.FreeRepository(r), 'TGitLibrary.FreeRepository');
      end;
   finally
      g.Free;
   end;
end;

procedure TTestCaseGitLibrary.TestGitDiffTreeToWorkdirWithIndex;
var
   g: TGitLibrary;
   r: PGitRepository;
   b: boolean;
   i: integer;
   p: PGitDiff;
   o: TGitDiffOptions;
   po: PGitDiffOptions;
   e: TGitError;
begin
   g := TGitLibrary.Create();
   try
      CheckTrue(g.Init, 'TGitLibrary.Init');
      CheckTrue(glsInitialized = g.Status, 'TGitLibrary.Status = glsInitialized');
      r := nil;
      b := g.OpenRepository(r, GetTestRepoPath);
      CheckTrue(b, 'TGitLibrary.OpenRepository');
      if b then begin
         ZeroMemory(@o, SizeOf(o));
         po := @o;
         i := g.FGitDiffInitOptions(po, 1);
         if g.GetLastError(e) then begin
            OutputDebugString(e.Message);
         end;
         p := nil;
         i := g.FGitDiffTreeToWorkdirWithIndex(p, r, nil, @po);
         if g.GetLastError(e) then begin
            OutputDebugString(e.Message);
         end;
         CheckEquals(0, i, 'TGitLibrary.FGitDiffTreeToWorkdirWithIndex');
         CheckTrue(g.FreeRepository(r), 'TGitLibrary.FreeRepository');
      end;
   finally
      g.Free;
   end;
end;


initialization

   RegisterTest(TTestCaseGitLibrary);
end.
