{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-03-27  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit VCSHelper.Settings.Frame.Mercurial;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   Forms,
   Controls,
   StdCtrls,
   ExtCtrls,
   IDEOptEditorIntf,
   IDEOptionsIntf;

type

   { TVCSSettingsMercurialUI }

   TVCSSettingsMercurialUI = class(TAbstractIDEOptionsEditor)
      cbShow: TCheckBox;
      cbtCommitOnProjectClose: TCheckBox;
      groupGeneral: TGroupBox;
      rgCloseOnEnd: TRadioGroup;
   private
      { private declarations }
   public
      { public declarations }
      constructor Create(AOwner: TComponent); override;
      function GetTitle: string; override;
      procedure Setup({%H-}ADialog: TAbstractOptionsEditorDialog); override;
      procedure ReadSettings({%H-}AOptions: TAbstractIDEOptions); override;
      procedure WriteSettings({%H-}AOptions: TAbstractIDEOptions); override;
      class function SupportedOptionsClass: TAbstractIDEOptionsClass; override;
   end;

implementation

uses
   VCSHelper.Options,
   VCSHelper.Strings;

{$R *.lfm}

{ TVCSSettingsMercurialUI }

constructor TVCSSettingsMercurialUI.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);
end;

function TVCSSettingsMercurialUI.GetTitle: string;
begin
   Result := 'Mercurial';
end;

procedure TVCSSettingsMercurialUI.Setup(ADialog: TAbstractOptionsEditorDialog);
begin
   cbShow.Caption := rsVCSOptionShowInMainMenu;
   cbtCommitOnProjectClose.Caption := rsVCSOptionCommitOnProjectClose;
end;

procedure TVCSSettingsMercurialUI.ReadSettings(AOptions: TAbstractIDEOptions);
begin
   cbShow.Checked := VCSSettings.MercurialSettings.ShowInMainMenu;
   cbtCommitOnProjectClose.Checked := VCSSettings.MercurialSettings.CommitOnProjectClose;
   rgCloseOnEnd.ItemIndex := VCSSettings.MercurialSettings.CloseOnEnd;
end;

procedure TVCSSettingsMercurialUI.WriteSettings(AOptions: TAbstractIDEOptions);
begin
   VCSSettings.MercurialSettings.ShowInMainMenu := cbShow.Checked;
   VCSSettings.MercurialSettings.CommitOnProjectClose := cbtCommitOnProjectClose.Checked;
   VCSSettings.MercurialSettings.CloseOnEnd := rgCloseOnEnd.ItemIndex;
end;

class function TVCSSettingsMercurialUI.SupportedOptionsClass: TAbstractIDEOptionsClass;
begin
   Result := TVCSSettings;
end;

initialization

   RegisterIDEOptionsEditor(VCSOptionGroup, TVCSSettingsMercurialUI, 4);

end.
