{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Setttings for LazGitPlugin.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017-2022 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-03-27  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit VCSHelper.Options;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   IniFiles,
   LazIDEIntf,
   IDEOptionsIntf;

type

   { TVCSBaseSettings }

   TVCSBaseSettings = class(TObject)
   private
      FCloseOnEnd: integer;
      FCommitOnProjectClose: boolean;
      FShowInMainMenu: boolean;
   protected
      function SectionName: string; virtual;
   public
      constructor Create;
      procedure SaveToIni(AIni: TCustomIniFile);
      procedure LoadFromIni(AIni: TCustomIniFile);
      property ShowInMainMenu: boolean read FShowInMainMenu write FShowInMainMenu;
      property CommitOnProjectClose: boolean read FCommitOnProjectClose write FCommitOnProjectClose;
      property CloseOnEnd: integer read FCloseOnEnd write FCloseOnEnd;
   end;

   { TBazaarSettings }

   TBazaarSettings = class(TVCSBaseSettings)
   protected
      function SectionName: string; override;
   end;

   { TCVSSettings }

   TCVSSettings = class(TVCSBaseSettings)
   protected
      function SectionName: string; override;
   end;

   { TGitSettings }

   TGitSettings = class(TVCSBaseSettings)
   protected
      function SectionName: string; override;
   end;

   { TMercurialSettings }

   TMercurialSettings = class(TVCSBaseSettings)
   protected
      function SectionName: string; override;
   end;

   { TSubversionSettings }

   TSubversionSettings = class(TVCSBaseSettings)
   protected
      function SectionName: string; override;
   end;

   { TVCSSettings }

   TVCSSettings = class(TAbstractIDEEnvironmentOptions) // TAbstractIDEEnvironmentOptions
   private
      FBazaarSettings: TBazaarSettings;
      FCVSSettings: TCVSSettings;
      FGitSettings: TGitSettings;
      FMercurialSettings: TMercurialSettings;
      FSubversionSettings: TSubversionSettings;
      function GetConfigFilename: string;
      procedure ApplyOptions;
   public
      constructor Create;
      destructor Destroy; override;
      procedure Read;
      procedure Write;
      procedure DoAfterWrite(Restore: boolean); override;
      class function GetGroupCaption: string; override;
      class function GetInstance: TAbstractIDEOptions; override;
      property BazaarSettings: TBazaarSettings read FBazaarSettings;
      property CVSSettings: TCVSSettings read FCVSSettings;
      property GitSettings: TGitSettings read FGitSettings;
      property MercurialSettings: TMercurialSettings read FMercurialSettings;
      property SubversionSettings: TSubversionSettings read FSubversionSettings;
   end;

function VCSSettings: TVCSSettings;

var
   VCSOptionGroup: integer;

implementation

uses
   Dialogs,
   VCSHelper.Menu;

var
   GVCSSettings: TVCSSettings = nil;

function VCSSettings: TVCSSettings;
begin
   if not Assigned(GVCSSettings) then begin
      GVCSSettings := TVCSSettings.Create;
   end;
   Result := GVCSSettings;
end;

{ TBazaarSettings }

function TBazaarSettings.SectionName: string;
begin
   Result := 'Bazaar';
end;

{ TCVSSettings }

function TCVSSettings.SectionName: string;
begin
   Result := 'CVS';
end;

{ TGitSettings }

function TGitSettings.SectionName: string;
begin
   Result := 'Git';
end;

{ TMercurialSettings }

function TMercurialSettings.SectionName: string;
begin
   Result := 'Mercurial';
end;

{ TSubversionSettings }

function TSubversionSettings.SectionName: string;
begin
   Result := 'Subversion';
end;

{ TVCSBaseSettings }

function TVCSBaseSettings.SectionName: string;
begin
   Result := 'Global';
end;

constructor TVCSBaseSettings.Create;
begin
   FShowInMainMenu := True;
   FCommitOnProjectClose := False;
end;

procedure TVCSBaseSettings.SaveToIni(AIni: TCustomIniFile);
begin
   try
      AIni.WriteBool(SectionName, 'ShowInMainMenu', FShowInMainMenu);
      AIni.WriteBool(SectionName, 'CommitOnProjectClose', FCommitOnProjectClose);
      AIni.WriteInteger(SectionName, 'CloseOnEnd', FCloseOnEnd);
   except
      on E: Exception do begin
         ShowMessage('There was an issue trying to save settings for ' + SectionName + ':'#13#10 + E.Message);
      end;
   end;
end;

procedure TVCSBaseSettings.LoadFromIni(AIni: TCustomIniFile);
begin
   try
      FShowInMainMenu := AIni.ReadBool(SectionName, 'ShowInMainMenu', True);
      FCommitOnProjectClose := AIni.ReadBool(SectionName, 'CommitOnProjectClose', False);
      FCloseOnEnd := AIni.ReadInteger(SectionName, 'CloseOnEnd', 0);
   except
      on E: Exception do begin
         ShowMessage('There was an issue trying to load settings for ' + SectionName + ':'#13#10 + E.Message);
      end;
   end;
end;

{ TVCSGitSettings }

function TVCSSettings.GetConfigFilename: string;
begin
   // While we would love to use the config path,
   // LazarusIDE currently is not available when first accessed!
   //if Assigned(LazarusIDE) then begin
   //   Result := IncludeTrailingPathDelimiter(LazarusIDE.GetPrimaryConfigPath) + 'LazGitHelper.ini';
   //end else begin
   //  Result := ExtractFilePath(ParamStr(0)) + 'LazGitHelper.ini';
   //end;
   Result := IncludeTrailingPathDelimiter(GetAppConfigDir(False)) + 'VCSHelper.ini';
end;

procedure TVCSSettings.ApplyOptions;

   procedure ProcessSystem(AHelper: TLazarusVCSHelper; ASettings: TVCSBaseSettings);
   begin
      try
         if Assigned(AHelper) and Assigned(ASettings) then begin
            AHelper.ToggleMenuVisible(ASettings.ShowInMainMenu);
         end;
      except
         on E: Exception do begin
            ShowMessage('There was an issue trying to apply settings for ' + ASettings.SectionName + ':'#13#10 + E.Message);
         end;
      end;
   end;

begin
   ProcessSystem(GBazaarHelper, FBazaarSettings);
   //ProcessSystem(GCVSHelper, FCVSSettings);
   ProcessSystem(GGitHelper, FGitSettings);
   ProcessSystem(GMercurialVersionHelper, FMercurialSettings);
   ProcessSystem(GSubVersionHelper, FSubversionSettings);
end;

constructor TVCSSettings.Create;
begin
   inherited Create;
   FBazaarSettings := TBazaarSettings.Create;
   FCVSSettings := TCVSSettings.Create;
   FGitSettings := TGitSettings.Create;
   FMercurialSettings := TMercurialSettings.Create;
   FSubversionSettings := TSubversionSettings.Create;
   Read;
   ApplyOptions;
end;

destructor TVCSSettings.Destroy;
begin
   Write;
   FBazaarSettings.Free;
   FCVSSettings.Free;
   FGitSettings.Free;
   FMercurialSettings.Free;
   FSubversionSettings.Free;
   inherited Destroy;
end;

procedure TVCSSettings.Read;
var
   mif: TMemIniFile;
begin
   mif := TMemIniFile.Create(GetConfigFilename);
   try
      FBazaarSettings.LoadFromIni(mif);
      FCVSSettings.LoadFromIni(mif);
      FGitSettings.LoadFromIni(mif);
      FMercurialSettings.LoadFromIni(mif);
      FSubversionSettings.LoadFromIni(mif);
   finally
      mif.Free;
   end;
end;

procedure TVCSSettings.Write;
var
   mif: TMemIniFile;
begin
   try
      mif := TMemIniFile.Create(GetConfigFilename);
      try
         FBazaarSettings.SaveToIni(mif);
         FCVSSettings.SaveToIni(mif);
         FGitSettings.SaveToIni(mif);
         FMercurialSettings.SaveToIni(mif);
         FSubversionSettings.SaveToIni(mif);
         mif.UpdateFile;
      finally
         mif.Free;
      end;
   except
      on E: Exception do begin
         ShowMessage('There was an issue trying to save VCSHelper settings to ' + GetConfigFilename + ':'#13#10 + E.Message);
      end;
   end;
end;

procedure TVCSSettings.DoAfterWrite(Restore: boolean);

begin
   inherited DoAfterWrite(Restore);
   Write;
   ApplyOptions;
end;

class function TVCSSettings.GetGroupCaption: string;
begin
   Result := 'Version Control System';
end;

class function TVCSSettings.GetInstance: TAbstractIDEOptions;
begin
   Result := VCSSettings;
end;

initialization

finalization

   GVCSSettings.Free;

end.
