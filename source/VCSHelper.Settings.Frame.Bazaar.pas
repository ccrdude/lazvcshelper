{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Lazarus IDE settings user interface for the Bazaar VCS.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-09  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit VCSHelper.Settings.Frame.Bazaar;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   Forms,
   Controls,
   StdCtrls,
   ExtCtrls,
   IDEOptEditorIntf,
   IDEOptionsIntf;

type

   { TVCSSettingsBazaarUI }

   TVCSSettingsBazaarUI = class(TAbstractIDEOptionsEditor)
      cbShow: TCheckBox;
      cbtCommitOnProjectClose: TCheckBox;
      groupGeneral: TGroupBox;
      rgCloseOnEnd: TRadioGroup;
   private
      { private declarations }
   public
      { public declarations }
      constructor Create(AOwner: TComponent); override;
      function GetTitle: string; override;
      procedure Setup({%H-}ADialog: TAbstractOptionsEditorDialog); override;
      procedure ReadSettings({%H-}AOptions: TAbstractIDEOptions); override;
      procedure WriteSettings({%H-}AOptions: TAbstractIDEOptions); override;
      class function SupportedOptionsClass: TAbstractIDEOptionsClass; override;
   end;

implementation

uses
   VCSHelper.Options,
   VCSHelper.Strings;

{$R *.lfm}

{ TVCSSettingsBazaarUI }

constructor TVCSSettingsBazaarUI.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);
end;

function TVCSSettingsBazaarUI.GetTitle: string;
begin
   Result := 'Bazaar';
end;

procedure TVCSSettingsBazaarUI.Setup(ADialog: TAbstractOptionsEditorDialog);
begin
   cbShow.Caption := rsVCSOptionShowInMainMenu;
   cbtCommitOnProjectClose.Caption := rsVCSOptionCommitOnProjectClose;
end;

procedure TVCSSettingsBazaarUI.ReadSettings(AOptions: TAbstractIDEOptions);
begin
   cbShow.Checked := VCSSettings.BazaarSettings.ShowInMainMenu;
   cbtCommitOnProjectClose.Checked := VCSSettings.BazaarSettings.CommitOnProjectClose;
   rgCloseOnEnd.ItemIndex := VCSSettings.BazaarSettings.CloseOnEnd;
end;

procedure TVCSSettingsBazaarUI.WriteSettings(AOptions: TAbstractIDEOptions);
begin
   VCSSettings.BazaarSettings.ShowInMainMenu := cbShow.Checked;
   VCSSettings.BazaarSettings.CommitOnProjectClose := cbtCommitOnProjectClose.Checked;
   VCSSettings.BazaarSettings.CloseOnEnd := rgCloseOnEnd.ItemIndex;
end;


class function TVCSSettingsBazaarUI.SupportedOptionsClass: TAbstractIDEOptionsClass;
begin
   Result := TVCSSettings;
end;

initialization

   RegisterIDEOptionsEditor(VCSOptionGroup, TVCSSettingsBazaarUI, 1);

end.
