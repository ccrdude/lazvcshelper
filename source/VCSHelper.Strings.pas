{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Strings for LazGitHelper plugin)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-03-27  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit VCSHelper.Strings;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils;

resourcestring
  rsVCSOptionShowInMainMenu = 'Show in main menu';
  rsVCSOptionCommitOnProjectClose = 'Commit changes to repository on closing project.';
  rsVCSHelperTitle = 'VCS Helper';
  rsVCSNoRepositoryFoundText = 'No %s repository found in path.';
  rsVCSActionCommit = '&Commit...';
  rsVCSActionPush = '&Push...';
  rsVCSActionPull = 'P&ull...';
  rsVCSActionRevertCurrent = '&Revert current...';
  rsVCSActionRevertSpecific = 'Revert %s...';
  rsVCSActionDiffAll = 'Diff all...';
  rsVCSActionDiffCurrent = '&Diff current...';
  rsVCSActionDiffSpecific = '&Diff %s...';
  rsVCSActionLog = '&Log...';
  rsVCSActionLogCurrent = '&Log current...';
  rsVCSActionLogSpecific = '&Log %s...';
  rsVCSActionBrowse = '&Browse...';
  rsVCSActionInit = '&Init...';
  rsVCSActionClone = 'C&lone...';
  rsVCSActionAbout = '&About...';
  rsVCSActionSettings = '&Settings...';
  rsVCSRevisionGraph = 'Re&vision Graph...';
  rsVCSActionCreateTag = 'Create &Tag...';

implementation

end.
