{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-23  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
// https://libgit2.github.com/libgit2/
// https://github.com/libgit2/GitForDelphi/blob/master/uGitForDelphi.pas
// *****************************************************************************
   )
}

unit PepiMK.VCS.Git.LibraryAPI;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   dynlibs,
   SysUtils;

type
   // https://libgit2.github.com/libgit2/#HEAD/type/git_repository
   PGitRepository = Pointer;

   TGitSubmoduleIgnoreT = (
      GIT_SUBMODULE_IGNORE_UNSPECIFIED,
      GIT_SUBMODULE_IGNORE_NONE,
      GIT_SUBMODULE_IGNORE_UNTRACKED,
      GIT_SUBMODULE_IGNORE_DIRTY,
      GIT_SUBMODULE_IGNORE_ALL);

   TGitStrArray = packed record
      Data: Pointer;
      Count: DWord; // size_t
   end;

   TGitOffT = Int64;

   TGitDiffOptions = packed record
      Version: cardinal; // unsigned int
      Flags: DWord; // uint32_t
      IgnoreSubModules: TGitSubmoduleIgnoreT;
      PathArray: TGitStrArray;
      NotifyCallback: Pointer;
      ProgressCallback: Pointer;
      Payload: Pointer;
      ContextLines: DWord; // uint32_t
      InterhunkLines: DWord; // uint32_t
      IDAbbrev: word; // uint16_t
      MaxSize: TGitOffT;
      OldPrefix: PAnsiChar;
      NewPrefix: PAnsiChar;
   end;
   PGitDiffOptions = ^TGitDiffOptions;
   PPGitDiffOptions = ^PGitDiffOptions;

   TGitError = packed record
      Message: PAnsiChar;
      Klass: Integer;
   end;
   PGitError = ^TGitError;

   PGitTree = Pointer;
   PGitDiff = Pointer;


   // https://libgit2.github.com/libgit2/#HEAD/group/giterr/giterr_last
   TGitErrorLast = function(                // const git_error * giterr_last();
      ): PGitError; stdcall;
   // https://libgit2.github.com/libgit2/#HEAD/group/repository/git_repository_open
   TGitRepositoryOpen = function(           // int git_repository_open(git_repository **out, const char *path);
      out AGitRepository: PGitRepository;   // git_repository **out,
      const APath: PAnsiChar                // const char *path
      ): integer; stdcall;
   // https://libgit2.github.com/libgit2/#HEAD/group/repository/git_repository_free
   TGitRepositoryFree = procedure(          // void git_repository_free(git_repository *repo);
      AGitRepository: PGitRepository        // git_repository *repo
      ); stdcall;
   TGitDiffInitOptions = function(          //  int git_diff_init_options(git_diff_options *opts, unsigned int version);
      const AOptions: PGitDiffOptions;            // git_diff_options *opts,
      AVersion: DWord                       // unsigned int version
      ): integer; stdcall;
   // https://libgit2.github.com/libgit2/#HEAD/group/diff/git_diff_tree_to_workdir_with_index
   // var AOptions: PGitDiffOptions    ->  (with nil:) invalid version 0 on git_diff_options
   // const AOptions: PGitDiffOptions  ->  (with nil:) SIGSEGV
   // AOptions: PGitDiffOptions        ->  (with nil:) SIGSEGV
   // AOptions: PGitDiffOptions        ->  (with empty record:) SIGSEGV
   // AOptions: PGitDiffOptions        ->  (with initialized record:) invalid version <pointer> on git_diff_options
   // var AOptions: TGitDiffOptions    ->  (with empty record): invalid version 0 on git_diff_options
   TGitDiffTreeToWorkdirWithIndex = function( // int git_diff_tree_to_workdir_with_index(git_diff **diff, git_repository *repo, git_tree *old_tree, const git_diff_options *opts);
      out ADiff: PGitDiff;                    // git_diff **diff,
      AGitRepository: PGitRepository;         // git_repository *repo,
      AOldTree: PGitTree;                     // git_tree *old_tree,
      AOptions: PGitDiffOptions               // const git_diff_options *opts
      ): integer; stdcall;

   TGitLibrary = class;

   TOnGitLibraryNotFoundEvent = function(AGitLibrary: TGitLibrary; var AFilename: WideString): boolean of object;

   TGitLibraryStatus = (glsUninitialized, glsInitialized, glsSuccess, glsLibraryNotFound);

   { TGitLibrary }

   TGitLibrary = class
   private
      FFilename: WideString;
      FHandle: TLibHandle;
      FOnGitLibraryNotFound: TOnGitLibraryNotFoundEvent;
      FStatus: TGitLibraryStatus;
      FGitRepositoryOpen: TGitRepositoryOpen;
      FGitRepositoryFree: TGitRepositoryFree;
      FGitErrorLast: TGitErrorLast;
   protected
      function InitializeImports: boolean;
   public
      FGitDiffInitOptions: TGitDiffInitOptions;
      FGitDiffTreeToWorkdirWithIndex: TGitDiffTreeToWorkdirWithIndex;
      constructor Create(ALibraryFilename: WideString = '');
      destructor Destroy; override;
      function GetDefaultFilename: WideString;
      function Init: boolean;
      function GetLastError(out AError: TGitError): boolean;
      function OpenRepository(out ARepository: PGitRepository; const APath: ansistring): boolean;
      function FreeRepository(ARepository: PGitRepository): boolean;
      property Status: TGitLibraryStatus read FStatus;
      property OnGitLibraryNotFound: TOnGitLibraryNotFoundEvent read FOnGitLibraryNotFound write FOnGitLibraryNotFound;
   end;


implementation

{ TGitLibrary }

function TGitLibrary.GetDefaultFilename: WideString;
begin
      {$IF Defined(Win64)}
   Result := 'libgit2_64.dll';
      {$ELSEIF Defined(Win32)}
   Result := 'libgit2_32.dll';
      {$ELSE}
   Result := 'libgit2';
      {$ENDIF}
end;

function TGitLibrary.InitializeImports: boolean;
begin
   Result := False;
   if (FHandle = 0) then begin
      FHandle := LoadLibrary(FFilename);
   end;
   if (FHandle = 0) and Assigned(FOnGitLibraryNotFound) then begin
      if FOnGitLibraryNotFound(Self, FFilename) then begin
         FHandle := LoadLibrary(FFilename);
      end;
   end;
   if (FHandle = 0) then begin
      FStatus := glsLibraryNotFound;
      Exit;
   end;
   FGitErrorLast := TGitErrorLast(GetProcAddress(FHandle, 'giterr_last'));
   FGitRepositoryOpen := TGitRepositoryOpen(GetProcAddress(FHandle, 'git_repository_open'));
   FGitRepositoryFree := TGitRepositoryFree(GetProcAddress(FHandle, 'git_repository_free'));
   FGitDiffInitOptions := TGitDiffInitOptions(GetProcAddress(FHandle, 'git_diff_init_options'));
   FGitDiffTreeToWorkdirWithIndex := TGitDiffTreeToWorkdirWithIndex(GetProcAddress(FHandle, 'git_diff_tree_to_workdir_with_index'));
   Result := Assigned(FGitErrorLast);
   // now repository specific
   Result := Result and Assigned(FGitRepositoryOpen) and Assigned(FGitRepositoryFree);
   // now diff specific
   Result := Result and Assigned(FGitDiffInitOptions) and Assigned(FGitDiffTreeToWorkdirWithIndex);
   if Result then begin
      FStatus := glsInitialized;
   end;
end;

constructor TGitLibrary.Create(ALibraryFilename: WideString);
begin
   if Length(ALibraryFilename) = 0 then begin
      FFilename := GetDefaultFilename;
   end else begin
      FFilename := ALibraryFilename;
   end;
   FHandle := 0;
   FStatus := glsUninitialized;
end;

destructor TGitLibrary.Destroy;
begin
   if FHandle > 0 then begin
      FreeLibrary(FHandle);
   end;
   inherited Destroy;
end;

function TGitLibrary.Init: boolean;
begin
   Result := InitializeImports;
end;

function TGitLibrary.GetLastError(out AError: TGitError): boolean;
var p: PGitError;
begin
   p := FGitErrorLast();
   Result := Assigned(p);
   if Result then begin
      AError := p^;
   end;
end;

function TGitLibrary.OpenRepository(out ARepository: PGitRepository; const APath: ansistring): boolean;
var
   i: integer;
begin
   Result := False;
   if not Assigned(FGitRepositoryOpen) then begin
      Exit;
   end;
   i := FGitRepositoryOpen(ARepository, PAnsiChar(APath));
   Result := (i = 0);
end;

function TGitLibrary.FreeRepository(ARepository: PGitRepository): boolean;
begin
   Result := False;
   if not Assigned(FGitRepositoryFree) then begin
      Exit;
   end;
   FGitRepositoryFree(ARepository);
   Result := True;
end;

end.
